package SnakeAndLadder;

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;



public class DiceRoleSnake extends Application{

    //Variable for random numbers
    public int rand;
    public Label randResult;

    //Circle position
    public int cirPos[][] = new int[10][10];
    public int ladderPosition[][] = new int[6][3];

    public static final int Tile_Size = 80;
    public static final int width = 10;
    public static final int height = 10;

    //Shape for players
    public Circle player1;
    public Circle player2;

    public int playerPosition1 = 1;
    public int playerPosition2 = 1;

    //TurnOn button
    public boolean player1Turn = true;
    public boolean player2Turn = true;

    //Player 1 coordinates
    public static int player1XPos = 40;
    public static int player1YPos = 760;

    //Player 2 coodinates
    public static int player2XPos = 40;
    public static int player2YPos = 760;

    //Position of where they start
    public int posCircle1 = 1;
    public int posCircle2 = 1;

    //Button for starting the game
    public boolean gameStart = false;
    public Button gameButton;

    private Group tileGroup = new Group();

    private Parent createContent(){
        StackPane root = new StackPane();
        root.setPrefSize(width * Tile_Size, (height * Tile_Size) + 80);
        root.getChildren().addAll(tileGroup);

        for(int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                Tile tile = new Tile(Tile_Size, Tile_Size);
                tile.setTranslateX(j * Tile_Size);
                tile.setTranslateY(i * Tile_Size);
                tileGroup.getChildren().add(tile);

                //Coordinates for x value
                cirPos[i][j] = j * (Tile_Size - 40);
            }
        }

        //Displaying coordinates for every position
        for(int i = 0; i < height; i++){
            for(int j = 0; j < width; j++){
                System.out.println(cirPos[i][j] + " ");
            }
            System.out.println();
        }

        //Shape for player 1
        player1 = new Circle(40);
        player1.setId("player1");
        player1.setFill(Color.AQUA);
        player1.setTranslateX(player1XPos);
        player1.setTranslateY(player1YPos);

        //Shape for player 2
        player2 = new Circle(40);
        player2.setId("player2");
        player2.setFill(Color.CHOCOLATE);
        player2.setTranslateX(player2XPos);
        player2.setTranslateY(player2YPos);

        //Button 2 (functionalities for button 2)
        Button button2 = new Button("Player2");
        button2.setTranslateX(700);
        button2.setTranslateY(820);
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //If GameStart is equal to true
                if(gameStart){
                    //If it is player 1 turn so it gets random number moves player to the position and second player cannot play
                    if(player2Turn){
                        getDiceValue();
                        randResult.setText(String.valueOf(rand));
                        movePlayer2();
                        translatePlayer(player2XPos, player2YPos, player2);
                        player2Turn = false;
                        player1Turn = true;
                        playerPosition2 += rand;

                        //When it comes to position 3 it goes automaticly to position 39
                        if(player2XPos == 200 && player2YPos == 760){
                            translatePlayer(player2XPos = 100, player2YPos = 520, player2);
                        }
                    }
                }
            }
        });

        //Button 1 (functionalities for button 1)
        Button button1 = new Button("Player1");
        button1.setTranslateX(80);
        button1.setTranslateY(820);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent ActionEvent) {
                //If GameStart is equal to true
                if(gameStart){
                    //If it is player 1 turn so it gets random number moves player to the position and second player cannot play
                    if(player1Turn){
                        getDiceValue();
                        randResult.setText(String.valueOf(rand));
                        movePlayer1();
                        translatePlayer(player1XPos, player1YPos, player1);
                        player1Turn = false;
                        player2Turn = true;
                        playerPosition1 += rand;

                        //When it comes to position 3 it goes automaticly to position 39
                        if(player1XPos == 200 && player1YPos == 760){
                            translatePlayer(player1XPos = 100, player1YPos = 520, player1);
                        }
                    }
                }
            }
        });

        //Button for the StartGame(making button where to be on the screen and the text)
        gameButton = new Button("Start Game");
        gameButton.setTranslateX(380);
        gameButton.setTranslateY(820);
        gameButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent ActionEvent) {
                gameButton.setText("Game Started");
                player1XPos = 40;
                player1YPos = 760;

                player2XPos = 40;
                player2YPos = 760;

                player1.setTranslateX(player1XPos);
                player1.setTranslateY(player1YPos);
                player2.setTranslateX(player2XPos);
                player2.setTranslateY(player2YPos);

                gameStart = true;
            }
        });

        randResult = new Label("O");
        randResult.setTranslateX(300);
        randResult.setTranslateY(820);

        //Getting image to the screen
        Image img = new Image("file: src/main/java/SnakeAndLadder/snake.jpg");
        ImageView snakeImage = new ImageView();
        snakeImage.setImage(img);
        snakeImage.setFitHeight(800);
        snakeImage.setFitWidth(800);

        //Adding everything to be visiable
        tileGroup.getChildren().addAll(snakeImage, player1, player2, button1, button2, gameButton, randResult);
        return root;
    }



    //Move player 1
    private void movePlayer1(){
        for(int i = 0; i < rand; i++){
            if(posCircle1 % 2 == 1){
                player1XPos += 80;
            }
            if(posCircle1 % 2 == 0){
                player1XPos -= 80;
            }
            if(player1XPos > 760){
                player1YPos -= 80;
                player1XPos -= 80;
                posCircle1++;
            }
            if(player1XPos < 40){
                player1YPos -= 80;
                player1XPos += 80;
                posCircle1++;
            }
            if(player1XPos < 30 || player1YPos < 30){
                player1XPos = 40;
                player1YPos = 40;
                gameStart = false;
                randResult.setText("Player One Won");
                gameButton.setText("Start Again");
            }
        }
    }

    //Move player 2
    private void movePlayer2(){
        for(int i = 0; i < rand; i++){
            if(posCircle2 % 2 == 1){
                player2XPos += 80;
            }
            if(posCircle2 % 2 == 0){
                player2XPos -= 80;
            }
            if(player2XPos > 760){
                player2YPos -= 80;
                player2XPos -= 80;
                posCircle2++;
            }
            if(player2XPos < 40){
                player2YPos -= 80;
                player2XPos += 80;
                posCircle2++;
            }
            if(player2XPos < 30 || player2YPos < 30){
                player2XPos = 40;
                player2YPos = 40;
                gameStart = false;
                randResult.setText("Player Two Won");
                gameButton.setText("Start Again");
            }
        }
    }
    //Get random dice number
    private void getDiceValue(){
        rand = (int) ((Math.random() * 6) + 1);
    }


    //Moves player to the x and y position
    private void translatePlayer(int x, int y, Circle b){
        TranslateTransition animate = new TranslateTransition(Duration.millis(1000), b);
        animate.setToX(x);
        animate.setToY(y);
        animate.setAutoReverse(false);
        animate.play();
    }

    @Override
    public void start(Stage primaryStage)  throws Exception{
        Scene scene = new Scene(createContent());
        primaryStage.setTitle("Snake and Ladders");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
